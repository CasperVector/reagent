#!/usr/bin/python2

import os
import struct
import zlib
try:
    from google.appengine.api import urlfetch
except ImportError:
    pass

def err_str(e):
    name, desc = type(e).__name__, str(e)
    return name + ": " + desc if desc else name

def my_assert(cond, err):
    if not cond:
        raise err

def read_len(f, l):
    data = f.read(l)
    my_assert(len(data) == l, ValueError("malformed message"))
    return data

def get_kvs(ss):
    l = []
    for s in ss:
        s = s.split(":", 1)
        my_assert(len(s) == 2, ValueError("malformed message"))
        l.append(s)
    return l

def chk_note(note, need, default):
    my_assert(all(k in note for k in need), ValueError("malformed message"))
    for k, v in default:
        if k in note:
            note[k] = int(note[k])
            my_assert(note[k] >= 0, ValueError("malformed message"))
        else:
            note[k] = v

def fmt_msg(note, head, body):
    zbody = zlib.compress(body)
    if len(zbody) < len(body):
        body, note["Zip"] = zbody, str(len(body))
    data = zlib.compress("\0\0".join("\0".join(
        "%s:%s" % (k, v) for k, v in d.items()
    ) for d in [note, head]))
    return struct.pack("!II", len(data), len(body)) + data + body

def get_msg(f):
    l = struct.unpack("!II", read_len(f, 8))
    data = zlib.decompress(read_len(f, l[0])).split("\0\0", 2)
    my_assert(len(data) == 2, ValueError("malformed message"))
    note, head = [dict(get_kvs(d.split("\0"))) if d else {} for d in data]
    return note, head, l[1]

def get_req(f):
    note, head, l = get_msg(f)
    password = os.environ.get("password", "")
    my_assert(
        not password or password == note.get("Pass"),
        RuntimeError("permission denied")
    )
    chk_note(note, ["Method", "Url"], [
        ("Zip", 0), ("Follow", 0), ("Check", 1), ("Wait", 25)
    ])
    my_assert(note["Wait"] <= 120, RuntimeError("permission denied"))
    data = read_len(f, l)
    if note["Zip"]:
        data = zlib.decompress(data)
        my_assert(len(data) == note["Zip"], ValueError("malformed message"))
    return note, head, data

def app(environ, start_response):
    start_response("200 OK", [])
    if environ["REQUEST_METHOD"] != "POST":
        return
    try:
        cfg, head, body = get_req(environ["wsgi.input"])
        note, resp = {}, urlfetch.fetch(
            cfg["Url"], body, cfg["Method"], head,
            allow_truncated = False, follow_redirects = cfg["Follow"],
            deadline = cfg["Wait"], validate_certificate = cfg["Check"]
        )
    except urlfetch.ResponseTooLargeError as e:
        note, resp = {"Short": "1"}, e.response
    except Exception as e:
        yield fmt_msg({"Status": err_str(e)}, {}, "")
        return
    note["Status"] = str(resp.status_code)
    yield fmt_msg(note, resp.headers, resp.content)

