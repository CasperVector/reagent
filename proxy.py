#!/usr/bin/python2

import cgi
import errno
import hashlib
import httplib
import os
import random
import re
import socket
import ssl
import sys
import threading
import time
import zlib
import BaseHTTPServer
import ConfigParser
import Queue
import SocketServer
from OpenSSL import crypto
from gae import err_str, my_assert, chk_note, fmt_msg, get_msg

class ReagentError(Exception):
    pass

def my_err(e):
    return repr(err_str(e))

def ignore_sepipe(f):
    def g(obj):
        try:
            f(obj)
        except socket.error as e:
            if e[0] != errno.EPIPE:
                raise
    return g

def get_range(ran):
    ran = ran.split(None, 2)
    my_assert(len(ran) == 2, ValueError("malformed Content-Range"))
    my_assert(ran[0] == "bytes", RuntimeError("unsupported Content-Range"))
    ran = ran[1].split("/", 2)
    my_assert(len(ran) == 2, ValueError("malformed Content-Range"))
    if ran[0] == "*":
        return 0, int(ran[1])
    ran = ran[0].split("-", 2)
    my_assert(len(ran) == 2, ValueError("malformed Content-Range"))
    return int(ran[0]), int(ran[1])

class MyHTTPSConn(httplib.HTTPSConnection):

    def __init__(self, *args, **kwargs):
        self._servername = kwargs.pop("name") if "name" in kwargs else None
        httplib.HTTPSConnection.__init__(self, *args, **kwargs)

    def connect(self):
        httplib.HTTPConnection.connect(self)
        self.sock = self._context.wrap_socket(
            self.sock, server_hostname = \
            self._tunnel_host or self._servername or self.host
        )

class CertUtility(object):

    def __init__(self, cn, keyfile, certdir):
        self.cn, self.keyfile, self.certdir = cn, keyfile, certdir
        self.bits, self.digest = 2048, "sha256"

    def mk_ca(self):
        key, cert = crypto.PKey(), crypto.X509()
        key.generate_key(crypto.TYPE_RSA, self.bits)
        subj = crypto.X509Req().get_subject()
        subj.commonName = self.cn
        cert.set_serial_number(0)
        cert.gmtime_adj_notBefore(0)
        cert.gmtime_adj_notAfter(60 * 60 * 24 * 3650)
        cert.set_issuer(subj)
        cert.set_subject(subj)
        cert.set_pubkey(key)
        cert.sign(key, self.digest)
        with open(self.keyfile, "wb") as f:
            f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))
            f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, key))

    def mk_cert(self, cn, keyfile):
        key, cert = crypto.PKey(), crypto.X509()
        key.generate_key(crypto.TYPE_RSA, self.bits)
        subj = crypto.X509Req().get_subject()
        subj.commonName = cn
        serial = self.hasher("%f|%s" % (time.time(), cn)).hexdigest()
        cert.set_serial_number(int(serial[:40], 16))
        cert.gmtime_adj_notBefore(-600)
        cert.gmtime_adj_notAfter(60 * 60 * 24 * 365)
        cert.set_issuer(self.subj)
        cert.set_subject(subj)
        cert.set_pubkey(key)
        cert.sign(self.key, self.digest)
        with open(keyfile, "wb") as f:
            f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))
            f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, key))

    def chk_ca(self):
        os.path.exists(self.keyfile) or self.mk_ca()
        os.path.exists(self.certdir) or os.makedirs(self.certdir)
        with open(self.keyfile, "rb") as f:
            s = f.read()
        cert = crypto.load_certificate(crypto.FILETYPE_PEM, s)
        self.key = crypto.load_privatekey(crypto.FILETYPE_PEM, s)
        self.subj = cert.get_subject()
        self.hasher = getattr(hashlib, self.digest)
        self.lock = threading.Lock()

    def get_cert(self, cn):
        parts = cn.split(".")
        if len(parts) > 2 and not re.match(r"[0-9]+", parts[-1]):
            cn = "." + cn.partition(".")[2]
            keyfile, cn = os.path.join(self.certdir, "pem," + cn), "*" + cn
        else:
            keyfile = os.path.join(self.certdir, "pem," + cn)
        with self.lock:
            os.path.exists(keyfile) or self.mk_cert(cn, keyfile)
        return keyfile

class GAEProxyHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    skips, rsize, frag, connect_timeout, protocol_version = [
        "Host", "Content-Length", "Connection", "Vary", "Via",
        "X-Forwarded-For", "Proxy-Authorization", "Proxy-Connection"
    ], 8192, 1024 * 1024, 36, "HTTP/1.1"
    gconn, q, https = None, None, False

    handle = ignore_sepipe(BaseHTTPServer.BaseHTTPRequestHandler.handle)
    finish = ignore_sepipe(BaseHTTPServer.BaseHTTPRequestHandler.finish)

    def send_response(self, code, message = None):
        if message == None:
            message = self.responses.get(code)
            message = message[0] if message else ""
        if self.request_version != "HTTP/0.9":
            self.wfile.write("%s %d %s\r\n" % (
                self.protocol_version, code, message
            ))

    def http_method(self):
        try:
            self.prep_http()
            ret = self.http_base()
            sys.stderr.write("%r(%r) %d(%d)\n" % (
                (self.command, self.path, ret, self.cnt)
            ))
        except ReagentError as e:
            self.close_connection = 1
            sys.stderr.write("%r(%r) %d(%s)\n" % (
                (self.command, self.path) + e.args
            ))
            if e[0] > 0:
                s = (
                    "<html>\n<head><title>reagent</title></head>\n"
                    "<body>\n<h1>%d %s</h1>\n%s\n</body>\n</html>\n"
                ) % (e[0], self.responses[e[0]][0], cgi.escape(e[1]))
                try:
                    self.send_response(e[0])
                    self.send_header("Content-Length", str(len(s)))
                    self.send_header("Connection", "close")
                    self.end_headers()
                    self.wfile.write(s)
                except:
                    pass

    do_CONNECT = do_GET = do_POST = do_HEAD = \
        do_PUT = do_DELETE = do_PATCH = http_method

    def prep_http(self):
        self.command == "CONNECT" and self.handle_strip()
        try:
            if self.https:
                if self.command == "CONNECT":
                    raise RuntimeError("nested CONNECT request")
                if not re.match(r"[A-Za-z]+:", self.path):
                    self.path = "https://" + self.headers["Host"] + (
                        "" if self.path.startswith("/") else "/"
                    ) + self.path
                my_assert(self.command in [
                    "GET", "POST", "HEAD", "PUT", "DELETE", "PATCH"
                ], RuntimeError("unsupported method after CONNECT"))
            l = int(self.headers.get("Content-Length", "0"))
            self.body = self.rfile.read(l) if l else ""
        except Exception as e:
            raise ReagentError(400, my_err(e))
        for h in self.skips:
            del self.headers[h]
        self.cfg = {"Method": self.command, "Url": self.path}
        if self.server.password:
            self.cfg["Pass"] = self.server.password
        if not self.gconn:
            self.gconn = [self.mk_conn()]

    def handle_strip(self):
        try:
            my_assert(not self.https, RuntimeError("nested CONNECT request"))
            m = re.match(r"(.*):[0-9]+$", self.path)
            if m:
                host = m.group(1)
                if host.startswith("["):
                    if not host.endswith("]"):
                        raise ValueError("malformed CONNECT request")
                    host = host[1 : -1]
            else:
                host = self.path
            if not host or re.search("[][/\0]", host):
                raise ValueError("malformed CONNECT request")
            self.https = True
        except Exception as e:
            raise ReagentError(400, my_err(e))
        try:
            self.send_response(200)
            self.end_headers()
            keyfile = self.server.certutil.get_cert(host)
            self.connection = ssl.wrap_socket(
                self.connection, keyfile = keyfile,
                certfile = keyfile, server_side = True
            )
            self.rfile = self.connection.makefile("rb", -1)
            self.wfile = self.connection.makefile("wb", 0)
            self.raw_requestline = self.rfile.readline(65537)
        except Exception as e:
            raise ReagentError(-1, my_err(e))
        ret = -1 if not self.raw_requestline else \
            (400 if len(self.raw_requestline) > 65536 else
            (-1 if not self.parse_request() else 0))
        if ret:
            self.command, self.path = "ERR", self.raw_requestline
            raise ReagentError(
                ret, "'ValueError: malformed request after CONNECT'"
            )

    def mk_conn(self, appids = None):
        return MyHTTPSConn(
            random.choice(self.server.hosts), timeout = self.connect_timeout,
            name = random.choice(self.server.fakeids) + ".appspot.com"
        ), random.choice(appids or self.server.appids)

    def http_base(self):
        try:
            resp, meta = self.get_resp()
        except Exception as e:
            raise ReagentError(502, "%s %s" % (self.gconn[0][1], my_err(e)))
        if not resp:
            raise ReagentError(501, "%s %r" % (self.gconn[0][1], meta))
        note, head, l = meta
        head, ran = self.proc_resp(note, head, l)
        self.cnt = 0
        try:
            self.send_response(note["Status"])
            for k, v in head.items():
                if k == "Set-Cookie":
                    [self.send_header(k, w) for w in re.sub(
                        r", *([-_a-zA-Z0-9]+=)", "\0" + r"\1", v
                    ).split("\0")]
                else:
                    self.send_header(k, v)
            self.end_headers()
            if note["Short"]:
                self.fetch_range(ran, head, (resp, l, note["Zip"]))
            else:
                self.fwd_resp(resp, l, note["Zip"])
            if "Content-Length" not in head:
                self.close_connection = 1
        except Exception as e:
            raise ReagentError(0, "[%d %d] %s" % (
                note["Status"], self.cnt, my_err(e)
            ))
        return note["Status"]

    def get_resp(self):
        msg = fmt_msg(self.cfg, self.headers, self.body)
        for i in range(1, -1, -1):
            self.gconn[0][0].request("POST", "/", msg, {
                "Host": self.gconn[0][1] + ".appspot.com"
            })
            resp = self.gconn[0][0].getresponse()
            if resp.status != 200:
                raise RuntimeError("%d: %s" % (resp.status, resp.read(2048)))
            note, head, l = get_msg(resp)
            chk_note(note, ["Status"], [("Zip", 0), ("Short", 0)])
            try:
                note["Status"] = int(note["Status"])
                head = dict((k.title(), v) for k, v in head.items())
                return resp, (note, head, l)
            except ValueError:
                if resp.read(self.rsize):
                    raise ValueError("malformed message")
                if note["Status"].startswith("OverQuotaError:") and i:
                    appids = [
                        appid for appid in self.server.appids
                        if appid != self.gconn[0][1]
                    ]
                    if appids:
                        self.gconn[0] = self.mk_conn(appids)
                        continue
                return None, note["Status"]

    def proc_resp(self, note, head, l):
        if "Connection" in head:
            head.pop("Connection")
        if head.get("Transfer-Encoding") == "chunked":
            head.pop("Transfer-Encoding")
        if note["Short"]:
            try:
                if head.get("Accept-Ranges") == "none":
                    raise RuntimeError("range requests unsupported by server")
                if not self.q:
                    self.q = Queue.Queue()
                    self.gconn.append(self.mk_conn())
                if "Content-Range" in head:
                    ran = get_range(head["Content-Range"])
                    if "Content-Length" in head:
                        head["Content-Length"] = str(ran[1] - ran[0] + 1)
                elif "Content-Length" in head and self.cfg["Method"] == "GET":
                    self.cfg["Method"] = "HEAD"
                    self.gconn.append(self.gconn.pop(0))
                    try:
                        resp, meta = self.get_resp()
                        my_assert(
                            resp and not resp.read(self.rsize) and
                            meta[0]["Status"] == note["Status"] and
                            not (meta[0]["Zip"] or meta[2]), AssertionError
                        )
                        head["Content-Length"] = meta[1]["Content-Length"]
                    except:
                        pass
                    self.gconn.append(self.gconn.pop(0))
                    self.cfg["Method"] = "GET"
                    ran = 0, int(head["Content-Length"]) - 1
                else:
                    raise RuntimeError("cannot guess content range")
            except Exception as e:
                ran = e
        else:
            ran = None
            if "Content-Length" not in head:
                s = note["Zip"] or l
                if s:
                    head["Content-Length"] = str(s)
        return head, ran

    def fwd_resp(self, resp, l, z):
        d = z and zlib.decompressobj()
        while l:
            n = min(self.rsize, l)
            data = resp.read(n)
            succ = len(data) == n
            if d:
                data = d.decompress(data)
                z -= len(data)
            if data:
                self.wfile.write(data)
                self.cnt += len(data)
            my_assert(succ, ValueError("truncated read"))
            l -= n
        if d:
            d.decompress(" ")
            my_assert(d.unused_data, ValueError("truncated zip stream"))
            data = d.flush()
            if data:
                self.wfile.write(data)
                self.cnt += len(data)
                z -= len(data)
            my_assert(not z, ValueError("malformed message"))
        if resp.read(self.rsize):
            raise ValueError("malformed message")

    def fetch_range(self, ran, head, prev):
        try:
            if isinstance(ran, Exception):
                raise ran
            l = prev[2] or prev[1]
            begin, end = ran
            begin, end = begin + l, end + 1
            my_assert(begin < end, ValueError("invalid content range"))
            l, self.cfg["Follow"] = int(0.9 * l), 1
            my_assert(l > self.frag, ValueError("fragment too short"))
        except:
            try:
                self.fwd_resp(*prev)
            except:
                pass
            raise
        while begin < end:
            if end - begin < l:
                l = end - begin
            threading.Thread(target = self.try_frag, args = (begin, l)).start()
            self.fwd_resp(*prev)
            prev = self.q.get()
            if isinstance(prev, Exception):
                raise prev
            begin += l
        self.fwd_resp(*prev)

    def try_frag(self, begin, l):
        ran = begin, begin + l - 1
        del self.headers["Range"]
        self.headers["Range"] = "bytes=%d-%d" % ran
        self.gconn.append(self.gconn.pop(0))
        try:
            try:
                resp, meta = self.get_resp()
            except Exception as e:
                s = err_str(e)
                raise RuntimeError("[502 %s] %s" % (self.gconn[0][1], s))
            if not resp:
                raise RuntimeError("[501 %s] %s" % (self.gconn[0][1], meta))
            note, head, sz = meta
            s = head.get("Content-Range")
            if s == None or get_range(s) != ran:
                raise ValueError("incorrect fragment range")
            if l != (note["Zip"] or sz):
                raise ValueError("incorrect fragment length")
            prev = resp, sz, note["Zip"]
        except Exception as e:
            prev = e
        self.q.put(prev)

class LocalProxyServer(SocketServer.ThreadingMixIn, BaseHTTPServer.HTTPServer):

    request_queue_size, allow_reuse_address, daemon_threads = 1024, True, True

    def __init__(self, cfg, RequestHandlerClass, bind_and_activate = True):
        BaseHTTPServer.HTTPServer.__init__(self, (
            cfg.get("listen", "ip"), cfg.getint("listen", "port")
        ), RequestHandlerClass, bind_and_activate)
        self.appids = cfg.get("gae", "appids").split("|")
        self.password = cfg.get("gae", "password")
        self.fakeids = cfg.get("gae", "fakeids").split("|")
        self.hosts = cfg.get("gae", "hosts").split("|")
        self.certutil = CertUtility(
            "reagent", cfg.get("ca", "key"), cfg.get("ca", "certs")
        )
        self.certutil.chk_ca()

def main(*args):
    cfg = ConfigParser.ConfigParser()
    cfg.read(*args)
    LocalProxyServer(cfg, GAEProxyHandler).serve_forever()

if __name__ == "__main__":
    main(sys.argv[1:])

